﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour {

    public bool walkable;
    public Vector3 worldPosition;
    public int gridX;
    public int gridY;

    public int gCost;
    public int hCost;
    public Node parent;

    

    void Awake()
    {
        //worldPosition = transform.localPosition;
        //gridX = (int)worldPosition.x / 10;
        //gridY = (int)worldPosition.z / 10; 
        worldPosition = transform.position;
        gridX = (int)transform.localPosition.x / 10;
        gridY = (int)transform.localPosition.z / 10; 
    }

    public int fCost
    {
        get
        {
            return gCost + hCost;
        }
    }
}
