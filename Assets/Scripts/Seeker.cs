﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seeker : MonoBehaviour
{
    public Vector3 nextLocation;
    public Animator anim;
    List<Node> path = new List<Node>();
    Node node;

    public int step;

    void Start()
    {
        step = 0;
        nextLocation = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (node != null)
        {
            nextLocation = node.transform.position;

            if (Vector3.Distance(gameObject.transform.position, nextLocation) < 0.1f)
            {
                nextNode();
            }
        }

        gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, nextLocation, Time.deltaTime * 5);
        gameObject.transform.LookAt(nextLocation, Vector3.up);
       
    }

    public void stop()
    {
        node = null;
        step = 0;
    }

    public void setPath(List<Node> path)
    {
        this.path = path;
        node = path[0];
        step = 0;
    }

    public void nextNode()
    {
        if (step < path.Count - 1)
        {
            anim.Play("CatWalk");
            step++;
            node = path[step];
            if (!node.walkable)
            {
                stop();
                return;
            }
        } else
        {
            path.Clear();
            node = null;
            step = 0;
        }
    }

}
