﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverFinger : MonoBehaviour {

    // Use this for initialization
    RaycastHit hit;
    Vector3 ray;

    public GameObject hoverObject;

    void Start () {
        ray = Vector3.down;
    }
	
	// Update is called once per frame
	void Update () {
        if (Physics.Raycast(transform.position, ray, out hit, 100))
        {
            //Debug.Log(hit.collider.transform.name);
            if (hit.transform.tag == "Grid")
            {
                //Debug.Log(hit.transform.position);
                hoverObject.transform.position = hit.transform.position;
                hoverObject.transform.position += new Vector3(0, 1, 0);
            }
            hoverObject.active = true;
        } else
        {
            hoverObject.active = false;
        }
    }
}
