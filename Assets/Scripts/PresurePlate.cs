﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresurePlate : MonoBehaviour
{

    // Use this for initialization
    Animator animator;
    bool  pressed = false;
    string name;

    public GameObject[] block;
    public bool trigerOnce;
    public bool heavy;
    Animator blockAnim;
    Node blockNode;

    void Awake()
    {
        //animator = GetComponent<Animator>();
        //blockAnim = block.GetComponent<Animator>();
        //blockNode = block.GetComponentInChildren<Node>();
    }

    void animate(Collider other)
    {
        foreach (GameObject b in block)
        {
            animator = GetComponent<Animator>();
            blockAnim = b.GetComponent<Animator>();
            blockNode = b.GetComponentInChildren<Node>();
            name = other.transform.name;
            animator.Play("Start");
            blockAnim.Play("Start");
            blockNode.walkable = true;
            pressed = true;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!pressed)
        {
            
            if (!heavy && other.gameObject.tag == "Hero")
            {
                animate(other);
            } else
            {
                
                if (other.gameObject.tag == "StoneBlock")
                {
                    Debug.Log(other.gameObject.tag);
                    animate(other);
                }
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (!trigerOnce && pressed && name == other.transform.name)
        {
            foreach (GameObject b in block)
            {
                animator = GetComponent<Animator>();
                blockAnim = b.GetComponent<Animator>();
                blockNode = b.GetComponentInChildren<Node>();
                animator.Play("Reverse");
                blockAnim.Play("Reverse");
                blockNode.walkable = false;
                pressed = false;
            }
        }

    }

}
