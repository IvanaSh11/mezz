﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
#endif

/*
 * [url]http://forum.unity3d.com/threads/24311-Replace-game-object-with-prefab/page2[/url]
 * */
#if UNITY_EDITOR
public class ReplaceGameObjects : ScriptableWizard
{

 
	public GameObject[] useGameObjects = new GameObject[1];
	public float[] probabilities = new float[1];	
	
	[MenuItem("Custom/Replace GameObjects")]
	static void CreateWizard()		
	{
		ScriptableWizard.DisplayWizard("Replace GameObjects", typeof(ReplaceGameObjects), "Replace");
	}

	void OnWizardCreate()
	{
		float totalProbs = 0;
		for(int i=0; i<probabilities.Length; ++i){
			totalProbs += probabilities[i];
		}
		if(totalProbs>0){
			for(int i=0; i<probabilities.Length; ++i){
				probabilities[i] = probabilities[i] / totalProbs;
			}
		}
		
		foreach (Transform t in Selection.transforms)
		{
			float rand = Random.value;
			float currentProb = 0;
			int idx = 0;
			for(int i=0; i<probabilities.Length; ++i) {
				currentProb += probabilities[i];
				if(rand <= currentProb) {
					idx = i;
					break;
				}
			}
			
			GameObject newObject = PrefabUtility.InstantiatePrefab(useGameObjects[idx]) as GameObject;
			Undo.RegisterCreatedObjectUndo(newObject, "created prefab");
			Transform newT = newObject.transform;

			newT.position = t.position;
			newT.rotation = t.rotation;
			newT.localScale = t.localScale;
			newT.parent = t.parent;	
		}

		foreach (GameObject go in Selection.gameObjects)
		{
			Undo.DestroyObjectImmediate(go);
		}
	}

}
#endif