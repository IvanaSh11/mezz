﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {

    // Use this for initialization
    public Animator animator;
    public int step = 1;

	void Start () {
		
	}

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Hero")
        {
            Debug.Log("CameraMove" + step.ToString());
            animator.Play("CameraMove" + step.ToString());
        }
    }
}
