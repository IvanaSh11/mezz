﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public LayerMask unwalkableMask;
    public Vector2 gridWorldSize;
    public Node[,] grid;

    public List<Node> path;

    public List<Node> gridList;

    public int gridSizeX, gridSizeY;

    void Start()
    {
        int x, y;
        grid = new Node[gridSizeX, gridSizeY];
        foreach (Node n in gridList)
        {
            x = n.gridX;
            y = n.gridY;

            grid[x, y] = n;
            //Debug.Log("x y" + x + " "  + y);
            if (grid[x, y].walkable)
            {
                grid[x, y].GetComponent<Renderer>().material.color = Color.green;
            } else
            {
                grid[x, y].GetComponent<Renderer>().material.color = Color.red;
            }
            //Debug.Log(grid[x, y].gridX);
            //Debug.Log(grid[x, y].gridY);
        }
    }

    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue;

                if (Mathf.Abs(x) == Mathf.Abs(y))
                    continue;
                //if(node.gridX + x >= 0 && node.gridY + y >= 0)
                //{
                //    Debug.Log("This:" + grid[node.gridX + x, node.gridY + y]);
                //    neighbours.Add(grid[node.gridX + x, node.gridY + y]);
                //}
                int checkX = node.gridX + x;
                int checkY = node.gridY + y;
                //Debug.Log("Neighbour:" + grid[node.gridX + x, node.gridY + y]);
                if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
                {
                    neighbours.Add(grid[checkX, checkY]);
                }

            }
        }

        return neighbours;
    }

    //return the node from world postion
    public Node NodeFromWorldPoint(Vector3 worldPosition)
    {
        //Debug.Log("pozicija " + worldPosition);


        //this is like this so when the postion is not / 10 (59, 49.9...)
        int x = (int)worldPosition.x / 10;
        int y = (int)worldPosition.z / 10;

        //foreach (Node n in gridList)
        //{
        //    if (n.worldPosition == worldPosition)
        //    {
        //        return n;
        //    }
        //}

        foreach (Node n in gridList)
        {
            if (n.gridX == x && n.gridY == y)
            {
                return n;
            }
        }

        path.Clear();
        Debug.Log("kade be");
        return null;

    }

    public void ClearPath()
    {
        Renderer renderer;
        //this doesn't delete the path just clears the color of the grid
        foreach (Node n in gridList)
        {
            renderer = n.GetComponent<Renderer>();
            if (renderer != null && n.walkable)
            {
                renderer.material.color = Color.green;
            }
            else
            {
                renderer.material.color = Color.red;
            }
        }
        //this deletes the path
        path.Clear();
    }
    //draw path
    public void DrawPath()
    {
        //ClearPath();
        if (grid != null)
        {
            foreach (Node n in grid)
            {
                if (path != null)
                {
                    if (path.Contains(n))
                    {
                        n.GetComponent<Renderer>().material.color = Color.black;
                    }
                }
            }
        }
    }
}
