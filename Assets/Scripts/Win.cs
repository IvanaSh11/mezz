﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class Win : MonoBehaviour {

    public GameObject winCanvas;

    public PostProcessingBehaviour postProces;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Hero")
        {
            winCanvas.SetActive(true);

            PostProcessingProfile profile = postProces.profile;
            DepthOfFieldModel.Settings g = profile.depthOfField.settings;
            g.aperture = 0.1f;
            profile.depthOfField.settings = g;

        }
    }
}
