﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresureCube : MonoBehaviour {

    // Use this for initialization
    Animator animator;

    public GameObject cube;
    Transform transform;
    Vector3 position;

    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        position = cube.GetComponent<Transform>().position;
    }
    void OnTriggerEnter(Collider other)
    {
        animator.Play("Start");
        Debug.Log(position + " " + cube.transform.position);
        if(cube.transform.position != position)
        {
            cube.GetComponent<Rigidbody>().isKinematic = true;
            cube.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            cube.transform.rotation = new Quaternion(0, 0, 0, 0);
            cube.transform.position = position;
            cube.GetComponent<Rigidbody>().isKinematic = false;
        }

    }
    void OnTriggerExit(Collider other)
    {
            animator.Play("Reverse");
       
    }
}
