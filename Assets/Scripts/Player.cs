﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    // Use this for initialization

    public GameObject target;
    public Pathfinding path;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Grid")
        {
            //Debug.Log(other.name);
            Renderer renderer = other.gameObject.GetComponent<Renderer>();
            renderer.material.color = Random.ColorHSV();
            target.transform.position = other.transform.position;
            path.Find();
        }
    }

}
