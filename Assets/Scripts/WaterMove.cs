﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterMove : MonoBehaviour {

    public float speed = 1;

	
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.position += Vector3.up * speed * Time.deltaTime;
	}
}
